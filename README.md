# redfish BM on Libvirt

(still wip)

## What this project does

To make it easy to test baremetal deployments in dev environments, this script creates libvirt domains adn attaches them to a sushy-emulator in order to expose a redfish api.

## How to use

- clone this repository
- edit the env variables at the begining of the script
- comment/uncomment the functions at the end of the script (provisioning, cleanup etc.)
- run the script
